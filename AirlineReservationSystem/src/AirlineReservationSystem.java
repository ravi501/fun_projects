
import java.util.ArrayList;

public class AirlineReservationSystem
{
    private FileUtility planeFile = new FileUtility();
    private String planeFileName = "plane.txt";
    
    private FileUtility reservationsFile = new FileUtility();
    private String reservationsFileName = "reservations.txt";
    
    private ArrayList<String> planeArray = new ArrayList<String>();
        
    private int ttlNbrFirstClass;
    private int ttlNbrBusinessClass;
    private int ttlNbrEconomyClass;
    private int ttlStandBy;
    
    final static int MAX_FIRST_CLASS = 6;
    final static int MAX_BUSINESS_CLASS = 12;
    final static int MAX_ECONOMY_CLASS = 30;
    final static int MAX_SEATS = 48;
    final static int MAX_SEATS_ROW = 6;
    
    final static String FIRST_CLASS_RESERVATION = "First Class";
    final static String BUSINESS_CLASS_RESERVATION = "Business Class";
    final static String ECONOMY_CLASS_RESERVATION = "Economy Class";
    final static String STANDBY_RESERVATION = "Standby";
    
    private String reservation;
    private String plane;
    private String reservationName;
    private char reservationType;
    private String reservationFlightNumber;
    
    /**
     * Constructor to initialize the airline reservation system and which prints that the system is starting
     */
    public AirlineReservationSystem(){
        System.out.println("Airline Reservation System is starting!");
    }
    
    /**
     * Starts the system by running the necessary functions required to store the plane information
     */
    public void run(){
        startUp();
        
        for(int i = 0; i < planeArray.size(); i++){
            ttlNbrFirstClass = 0;
            ttlNbrBusinessClass = 0;
            ttlNbrEconomyClass = 0;
            ttlStandBy = 0;
            if(reservation != null){
                processFlightReservations(i);
            }
            displayPlaneSummary(i);
        }
        wrapUp();
    }
    
    /**
     * Opens the plane.txt and reservations.txt files
     */
    private void startUp(){
        planeFile.openReadFile(planeFileName);
        reservationsFile.openReadFile(reservationsFileName);
        loadPlaneArray();
        reservation = reservationsFile.getNextLine();
    }
    
    /**
     * Loads the details of each plane from the plane file into an array
     */
    private void loadPlaneArray()
    {
        plane = planeFile.getNextLine();
        while(plane != null){
            planeArray.add(plane);
            plane = planeFile.getNextLine();
        }
    }
    
   /**
     * Compares the Flight numbers 
     * @param x - Current filling flight number
     */
    private void processFlightReservations(int x){
                
        String planeFlightNumber = getPlaneFlightNumber(planeArray.get(x));
        String reservationFlightNumber = getReservationFlightNumber(reservation);
        
        while((reservation != null) && (planeFlightNumber.equals(reservationFlightNumber))){
            processReservation();
            reservation = reservationsFile.getNextLine();
            if(reservation != null){
                reservationFlightNumber = getReservationFlightNumber(reservation);
            }
        }
    }
    
    /**
     * Processes the Reservation
     */
    private void processReservation(){
        
        reservationFlightNumber = getReservationFlightNumber(reservation);
        reservationType = getReservationType(reservation);
        reservationName = getReservationName(reservation);
        
        switch(reservationType){
            case 'F':
                if(ttlNbrFirstClass < MAX_FIRST_CLASS){
                    ttlNbrFirstClass++;
                    printReservation(reservationFlightNumber, FIRST_CLASS_RESERVATION, reservationName);
                    
                }
                else{
                    processStandbyReservation(reservationName, reservationFlightNumber);
                }
                break;
                
           case 'B':
                if(ttlNbrBusinessClass < MAX_BUSINESS_CLASS){
                    ttlNbrBusinessClass++;
                    printReservation(reservationFlightNumber, BUSINESS_CLASS_RESERVATION, reservationName);
                }
                else{
                    processStandbyReservation(reservationName, reservationFlightNumber);
                }
                break;
           
           case 'E':
                if(ttlNbrEconomyClass < MAX_ECONOMY_CLASS){
                    ttlNbrEconomyClass++;
                    printReservation(reservationFlightNumber, ECONOMY_CLASS_RESERVATION, reservationName);
                }
                else{
                    processStandbyReservation(reservationName, reservationFlightNumber);
                }
                break;
        
        }
    }
    
    /**
     * Processes the Standby Reservations 
     * @param reservationName
     * @param flightNumber
     */
    private void processStandbyReservation(String reservationName, String flightNumber){
        ttlStandBy++;
        printReservation(reservationName, STANDBY_RESERVATION, flightNumber);
        
    }
    
    /**
     * Prints the flight reservation
     * @param flightNumber
     * @param reservationType
     * @param customerName
     */
    private void printReservation(String flightNumber, String reservationType, String customerName){
        System.out.println("*************************************");
        System.out.println("Flight number:           " + flightNumber);
        System.out.println("Reservation Type:        " + reservationType);
        System.out.println("Customer:                " + customerName);
    }
    
    /**
     * Displays the Plane Summary
     * @param x
     */
    private void displayPlaneSummary(int x)
    {
        System.out.println("Plane Reservation Details");
        System.out.println("------------------------------------------------");
        System.out.println("Flight Number:          " + getPlaneFlightNumber(planeArray.get(x)));
        System.out.println("Departure Airport:      " + getPlaneDepartureAirport(planeArray.get(x)));
        System.out.println("Arrival Airport:        " + getPlaneArrivalAirport(planeArray.get(x)));
        System.out.println("Standby Reservations:   " + ttlStandBy);
        System.out.println("------------------------------------------------");
        System.out.println("Reservations:");
        printClassSeats(ttlNbrFirstClass, MAX_FIRST_CLASS);
        printClassSeats(ttlNbrBusinessClass, MAX_BUSINESS_CLASS);
        printClassSeats(ttlNbrEconomyClass, MAX_ECONOMY_CLASS);
    }
    
    /**
     * Prints all the Economy seats reserved for a particular flight
     * @param ttlEcnmy
     * @param maxEcnmy
     */
    private void printClassSeats(int ttlClassSeats, int maxClassSeats){
        int rows = maxClassSeats/MAX_SEATS_ROW;
        for(int i = 0; i < rows ; i++){
            System.out.print("-");
            for(int j = 0; j < MAX_SEATS_ROW; j++){
                if(ttlClassSeats > 0){
                    System.out.print("XX-");
                    ttlClassSeats--;
                }
                else{
                    System.out.print("__-");
                }
            }
            System.out.println();
        }
        
    }

    /**
     * Displays the message that the Airline Reservation system is ending
     */
    private void wrapUp(){
        System.out.println("Airline Reservation system is ending!");
    }

    /**
     * Extracts the reservation's customer name from the reservation record.
     * 
     * @param record
     * @return String - the customer name.
     */
    private String getReservationName(String record)
    {
        return record.substring(5);
    }

    /**
     * Extracts the reservation type from a reservation record.
     * 
     * @param record
     * @return char - the character reservation type code.
     */
    private char getReservationType(String record)
    {
        return record.charAt(0);
    }

    /**
     * Extracts the 4-character flight number from a reservation number.
     * 
     * @param record
     * @return String - the flight number.
     */
    private String getReservationFlightNumber(String record){
        return record.substring(1, 5);
    }

    /**
     * Extracts the 4-character flight number from a plane record.
     * 
     * @param record
     * @return String - the flight number.
     */
    private String getPlaneFlightNumber(String record){
        return record.substring(0, 4);
    }

    /**
     * Extracts the plane departure airport 3-character code from the plane
     * record.
     * 
     * @param record
     * @return String - the departure airport code.
     */
    private String getPlaneDepartureAirport(String record){
        return record.substring(4, 7);
    }

    /**
     * Extracts the plane arrival airport 3-character code from the plane
     * record.
     * 
     * @param record
     * @return String - the arrival airport code.
     */
    private String getPlaneArrivalAirport(String record){
        return record.substring(7, 10);
    }
}
