public class EarthquakeClaim extends Claim
{
    public final int MAX_EARTHQUAKE_CLAIM  = 2500;

    public final int EARTHQUAKE_DEDUCTIBLE = 1000;

    public void adjustClaim()
    {
        if ( getAmount() > MAX_EARTHQUAKE_CLAIM )
        {
            setAmount( EARTHQUAKE_DEDUCTIBLE );
        }
        setAmount( getAmount() - EARTHQUAKE_DEDUCTIBLE );
    }
}
