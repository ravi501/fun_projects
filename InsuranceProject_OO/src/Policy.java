public class Policy implements IPolicy
{
    // The Policy Class should consist of the following behaviors & methods. Use
    // the method
    // names below, because it will make it easier later for debugging purposes.

    private int    begTtlNbrOfClaims;
    private double begTtlAmtOfClaims;

    private int    endTtlNbrOfClaims;
    private double endTtlAmtOfClaims;

    private String policyNbr;

    public int getBegTtlNbrOfClaims()
    {
        return begTtlNbrOfClaims;
    }

    public void setBegTtlNbrOfClaims( int begTtlNbrOfClaims )
    {
        this.begTtlNbrOfClaims = begTtlNbrOfClaims;
    }

    public double getBegTtlAmtOfClaims()
    {
        return begTtlAmtOfClaims;
    }

    public void setBegTtlAmtOfClaims( double begTtlAmtOfClaims )
    {
        this.begTtlAmtOfClaims = begTtlAmtOfClaims;
    }

    public int getEndTtlNbrOfClaims()
    {
        return endTtlNbrOfClaims;
    }

    public void setEndTtlNbrOfClaims( int endTtlNbrOfClaims )
    {
        this.endTtlNbrOfClaims = endTtlNbrOfClaims;
    }

    public double getEndTtlAmtOfClaims()
    {
        return endTtlAmtOfClaims;
    }

    public void setEndTtlAmtOfClaims( double endTtlAmtOfClaims )
    {
        this.endTtlAmtOfClaims = endTtlAmtOfClaims;
    }

    public String getPolicyNbr()
    {
        return policyNbr;
    }

    public void setPolicyNbr( String policyNbr )
    {
        this.policyNbr = policyNbr;
    }

    public void build( PolicyParser policyLine )
    {
        setPolicyNbr( policyLine.getPolicyNumber() );
        setBegTtlAmtOfClaims( policyLine.getAmtOfClaims() );
        setBegTtlNbrOfClaims( policyLine.getNbrOfClaims() );
        setEndTtlAmtOfClaims( policyLine.getAmtOfClaims() );
        setEndTtlNbrOfClaims( policyLine.getNbrOfClaims() );
    }

    private double calcBegAvgAmtPerClaim()
    {
        double begAvgAmtPerClaim = 0;

        if ( begAvgAmtPerClaim > 0 )
        {
            begAvgAmtPerClaim = begTtlAmtOfClaims / begTtlNbrOfClaims;
        }

        return begAvgAmtPerClaim;

    }

    private double calcEndAvgAmtPerClaim()
    {
        double endAvgAmtPerClaim = 0;

        if ( endTtlNbrOfClaims > 0 )
        {
            endAvgAmtPerClaim = endTtlAmtOfClaims / endTtlNbrOfClaims;
        }

        return endAvgAmtPerClaim;
    }

    public void print()
    {
        System.out.println();
        System.out.println( "Policy#:     " + policyNbr );
        System.out.println( "Beg Total#:  " + begTtlNbrOfClaims );
        System.out.println( "Beg Total$:  " + begTtlAmtOfClaims );
        System.out.println( "Beg Avg$:    " + calcBegAvgAmtPerClaim() );
        System.out.println( "End Total#:  " + endTtlNbrOfClaims );
        System.out.println( "End Total$:  " + endTtlAmtOfClaims );
        System.out.println( "End Avg$:    " + calcEndAvgAmtPerClaim() );
    }
}
