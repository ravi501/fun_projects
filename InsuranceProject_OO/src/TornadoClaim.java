public class TornadoClaim extends Claim
{
    public final int TORNADO_DEDUCTIBLE = 2500;

    public void adjustClaim()
    {
        setAmount( getAmount() - TORNADO_DEDUCTIBLE );
    }
}
