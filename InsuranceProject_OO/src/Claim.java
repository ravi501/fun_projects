import java.util.Date;


public abstract class Claim implements IClaim
{
    // The Claim Class should consist of the following behaviors & methods. Use
    // the method
    // names below, because it will make it easier later for debugging purposes.

    private Date   claimDate;
    private double amount;
    private char   claimType;
    private Policy policy;

    public Date getClaimDate()
    {
        return claimDate;
    }

    public void setClaimDate( Date claimDate )
    {
        this.claimDate = claimDate;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        if ( amount < 0 )
        {
            amount = 0;
        }
        this.amount = amount;
    }

    public char getClaimType()
    {
        return claimType;
    }

    public void setClaimType( char claimType )
    {
        this.claimType = claimType;
    }

    public Policy getPolicy()
    {
        return policy;
    }

    public void setPolicy( Policy policy )
    {
        this.policy = policy;
    }

    public void build( ClaimParser dataRow )
    {
        setAmount( dataRow.getAmount() );
        setClaimDate( dataRow.getClaimDate() );
        setClaimType( dataRow.getClaimType() );
        setPolicy( InsuranceSystem.getInstance().getPolicy(
                dataRow.getPolicyNumber() ) );
    }

    public void process()
    {
        adjustClaim();

        policy.setEndTtlNbrOfClaims( policy.getEndTtlNbrOfClaims() + 1 );
        policy.setEndTtlAmtOfClaims( policy.getEndTtlAmtOfClaims() + amount );
    }

    public static Claim getClaim( ClaimParser cp )
    {
        Claim myClaim = null;

        switch ( cp.getClaimType() )
        {
        // Hail
            case 'H':
            {
                myClaim = new HailClaim();

                break;
            }
            // Tornado
            case 'T':
            {
                myClaim = new TornadoClaim();
                break;
            }
            // Earthquake
            case 'E':
            {
                myClaim = new EarthquakeClaim();
                break;
            }
            // Hurricane
            case 'U':
            {
                myClaim = new HurricaneClaim();

                break;
            }
            // Fire
            case 'F':
            {
                myClaim = new FireClaim();

                break;
            }
            // Flood
            case 'L':
            {
                myClaim = new FloodClaim();
                break;
            }
            // Burglary
            case 'B':
            {
                myClaim = new BurglaryClaim();
                break;
            }
            // Accident
            case 'A':
            {
                myClaim = new AccidentClaim();
                break;
            }
            default:
                System.out.println( "For policy " + cp.getPolicyNumber()
                        + ", invalid claim type = " + cp.getClaimType() );

        }

        return myClaim;
    }
}
