public class PolicyParser
{
    String policyLine = null;

    public PolicyParser( String fileLine )
    {
        this.policyLine = fileLine;
    }

    public String getPolicyNumber()
    {
        return policyLine.substring( 0, 15 );
    }

    public int getNbrOfClaims()
    {
        int nbrOfClaims = 0;

        try
        {
            nbrOfClaims = Integer.parseInt( policyLine.substring( 15, 17 ) );
        }
        catch ( ArithmeticException e )
        {
            System.out.println( "ERROR OCCURRED PARSING nbrOfClaims" );
            e.printStackTrace();
        }

        return nbrOfClaims;
    }

    public double getAmtOfClaims()
    {
        double amtOfClaims = 0.0;

        try
        {
            amtOfClaims = (Double.parseDouble( policyLine.substring( 17, 28 ) ) / 100);
        }
        catch ( ArithmeticException e )
        {
            System.out.println( "ERROR OCCURRED PARSING amtOfClaims" );
            e.printStackTrace();
        }

        return amtOfClaims;
    }
}
