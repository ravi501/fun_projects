import java.util.ArrayList;


public class InsuranceSystem
{
    // The InsuranceSystem Class should consist of the following behaviors &
    // methods. Use the method
    // names below, because it will make it easier later for debugging purposes.

    private ArrayList< Policy >    policyArray    = new ArrayList< Policy >();

    // Policy file
    private FileUtility            policyFile     = new FileUtility();
    private String                 policyFileName = "policy.txt";

    // Claim file
    private FileUtility            claimFile      = new FileUtility();
    private String                 claimFileName  = "claim.txt";

    private static InsuranceSystem instance;

    private InsuranceSystem()
    {

    }

    public static InsuranceSystem getInstance()
    {
        if ( instance == null )
        {
            instance = new InsuranceSystem();
        }

        return instance;
    }

    public ArrayList< Policy > getPolicyArray()
    {
        return policyArray;
    }

    public void setPolicyArray( ArrayList< Policy > policyArray )
    {
        this.policyArray = policyArray;
    }

    public void process()
    {
        policyFile.openReadFile( policyFileName );
        loadPolicyArray();
        claimFile.openReadFile( claimFileName );
        processClaims();
        printUpdatedPolicies();
    }

    private void loadPolicyArray()
    {
        String policyData = policyFile.getNextLine();

        while ( policyData != null )
        {
            PolicyParser ipp = new PolicyParser( policyData );
            Policy policy = new Policy();
            policy.build( ipp );
            policyArray.add( policy );
            policyData = policyFile.getNextLine();
        }
    }

    public Policy getPolicy( String policyNumber )
    {
        Policy policy = null;

        for ( int i = 0; i < policyArray.size(); i++ )
        {
            Policy insPol = policyArray.get( i );
            if ( insPol.getPolicyNbr().equals( policyNumber ) )
            {
                policy = insPol;
                break;
            }
        }
        return policy;
    }

    private void processClaims()
    {
        String claimData = claimFile.getNextLine();

        while ( claimData != null )
        {
            ClaimParser cp = new ClaimParser( claimData );
            Claim claim = Claim.getClaim( cp );

            if ( claim != null )
            {
                claim.build( cp );
                claim.process();
            }
            claimData = claimFile.getNextLine();
        }
    }

    private void printUpdatedPolicies()
    {
        for ( int i = 0; i < policyArray.size(); i++ )
        {
            Policy policy = policyArray.get( i );

            policy.print();
        }
    }

}
