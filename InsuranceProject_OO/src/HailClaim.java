public class HailClaim extends Claim
{
    public final int MAX_HAIL_CLAIM  = 5000;
    public final int HAIL_DEDUCTIBLE = 1000;

    public void adjustClaim()
    {
        if ( getAmount() > MAX_HAIL_CLAIM )
        {
            setAmount( MAX_HAIL_CLAIM );
        }
        setAmount( getAmount() - HAIL_DEDUCTIBLE );
    }
}
