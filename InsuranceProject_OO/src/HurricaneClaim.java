public class HurricaneClaim extends Claim
{
    public final int HURRICANE_DEDUCTIBLE = 2500;

    public void adjustClaim()
    {
        setAmount( getAmount() - HURRICANE_DEDUCTIBLE );
    }
}
