public class OpenAccountTransaction extends Transaction
{
    /**
     * Method to open a new account
     */
    public boolean process()
    {
        boolean isTransactionComplete = true;

        // Get the reference of the bank system for adding new account object to
        // the array list of accounts
        BankSystem bank = BankSystem.getInstance();

        // Create a new account reference type
        Account account = new Account();

        System.out.println( "Creating a new account "
                + getTransactionAccountNbr()
                + " and setting its status to active" );

        // Build the account object with new account values
        account.build( getTransactionAccountNbr(), getAmount() );

        setFromAccount( account );

        isTransactionComplete = bank.addNewAccount( account );

        return isTransactionComplete;
    }

    public String transactionType()
    {
        return "Open";
    }
}
