public class Account
{
    // Account class stores individual accounts in the banking system

    // Stores the account number
    private String    accountNbr;

    // Stores the beginning and ending balances of account
    private double    begTtlBalance;
    private double    endTtlBalance;

    // Stores the total number of transactions done on the account
    private int       ttlNbrOfTransactions;

    // Stores the account status - whether the account is active or closed
    private char      status;

    // Stores the account active and closed status
    final static char ACCOUNT_ACTIVE_STATUS = 'A';
    final static char ACCOUNT_CLOSED_STATUS = 'C';

    /**
     * Getter method for account number
     * 
     * @return
     */
    public String getAccountNbr()
    {
        return accountNbr;
    }

    /**
     * Setter method for account number
     * 
     * @param accountNbr
     */
    public void setAccountNbr( String accountNbr )
    {
        this.accountNbr = accountNbr;
    }

    /**
     * Getter method for beginning total balance
     * 
     * @return - Returns beginning total balance
     */
    public double getBegTtlBalance()
    {
        return begTtlBalance;
    }

    /**
     * Setter method for beginning total balance
     * 
     * @param begTtlBalance
     */
    public void setBegTtlBalance( double begTtlBalance )
    {
        this.begTtlBalance = begTtlBalance;
    }

    /**
     * Getter method for end total balance
     * 
     * @return
     */
    public double getEndTtlBalance()
    {
        return endTtlBalance;
    }

    /**
     * Setter method for end total balance
     * 
     * @param endTtlBalance
     */
    public void setEndTtlBalance( double endTtlBalance )
    {
        this.endTtlBalance = endTtlBalance;
    }

    /**
     * Getter method for total number of transactions
     * 
     * @return
     */
    public int getTtlNbrOfTransactions()
    {
        return ttlNbrOfTransactions;
    }

    /**
     * Setter method for total number of transactions
     * 
     * @param ttlNbrOfTransactions
     */
    public void setTtlNbrOfTransactions( int ttlNbrOfTransactions )
    {
        this.ttlNbrOfTransactions = ttlNbrOfTransactions;
    }

    /**
     * Account status getter method
     * 
     * @return
     */
    public char getStatus()
    {
        return status;
    }

    /**
     * Account status setter method
     * 
     * @param status
     */
    public void setStatus( char status )
    {
        this.status = status;
    }

    /**
     * Method for initializing the member of Account class
     * 
     * @param accountLine
     *            - Account data read using Account parser
     */
    public void build( AccountParser accountLine )
    {
        setAccountNbr( accountLine.getAccountNumber() );
        setBegTtlBalance( accountLine.getBalance() );
        setEndTtlBalance( accountLine.getBalance() );
        setStatus( accountLine.getStatus() );
    }

    /**
     * Method for initializing a newly created account
     * 
     * @param accountNumber
     *            - Account number of the newly created account
     * @param accountBalance
     *            - Account balance of the newly created account
     */
    public void build( String accountNumber, double accountBalance )
    {
        setAccountNbr( accountNumber );
        setBegTtlBalance( 0 );
        setEndTtlBalance( accountBalance );
        setStatus( ACCOUNT_ACTIVE_STATUS );
    }

    /**
     * Method for printing the individual account details
     */
    public void print()
    {
        System.out.println();
        System.out.println( "Account#:                       "
                + getAccountNbr() );
        System.out.println( "Beginning balance:              $"
                + getBegTtlBalance() );
        System.out.println( "Ending balance:                 $"
                + getEndTtlBalance() );
        System.out.println( "Total number of transactions =  "
                + getTtlNbrOfTransactions() );
    }

}
