public class CloseAccountTransaction extends Transaction
{
    /**
     * Process method to process the closing of an existing account
     */
    public boolean process()
    {
        boolean isTransactionComplete = isAccountFound( getFromAccount() );

        // Check if the account is present or not
        if ( isTransactionComplete )
        {
            // Check if the end balance of the account is zero or not
            if ( !updateStatus( Account.ACCOUNT_CLOSED_STATUS, getFromAccount() ) )
            {
                // If the end balance is not zero, display an error message and
                // update the variable to false i.e., transaction is not
                // complete
                System.out
                        .println( "Account "
                                + getTransactionAccountNbr()
                                + " could not be closed because of non-zero balance in the account" );
                isTransactionComplete = false;
            }
        }
        return isTransactionComplete;
    }

    public String transactionType()
    {
        return "Close";
    }
}
