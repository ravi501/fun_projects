public class DepositTransaction extends Transaction
{
    /**
     * Processes the deposit transaction on the account
     */
    public boolean process()
    {
        boolean isTransactionComplete = isAccountFound( getFromAccount() );

        // Checks if the account is present or not
        if ( isTransactionComplete )
        {
            // If the account is present, update the end balance of the account
            // by the transaction amount
            getFromAccount().setEndTtlBalance(
                    getFromAccount().getEndTtlBalance()
                            + getAmount() );

            // And also, set the account status to active
            updateStatus( Account.ACCOUNT_ACTIVE_STATUS, getFromAccount() );
        }
        return isTransactionComplete;
    }

    public String transactionType()
    {
        return "Deposit";
    }
}
