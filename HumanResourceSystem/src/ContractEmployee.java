public class ContractEmployee extends Employee
{
    /**
     * Process benefit processes the benefits corresponding to the Contract
     * employees
     */
    public void build( EmployeeParser parser )
    {
        // Calls the load function to load the basic attributes of the employee
        super.build( parser );

        // Sets the contractor specific attributes
        setEmployeeType( CONTRACTOR_EMPLOYEE_TYPE );
    }

    @Override
    public Double calculateInsurance()
    {
        return null;
    }

}
