public class FullTimeEmployee extends Employee
{
    // Constants specific to FullTimeEmployees
    final static double MIN_INSURANCE_AMOUNT         = 50000;
    final static double MAX_INSURANCE_AMOUNT         = 400000;
    final static double INSURANCE_MULTIPLYING_FACTOR = 2.25;

    /**
     * Processes the benefits common to all the full-time employees
     */
    public void build( EmployeeParser parser )
    {
        // Calls the load function of the employee to load the basic attributes
        // of the employee
        super.build( parser );

        setRetirementEligibility( Boolean.TRUE );
    }

    /**
     * Calculates the insurance amount. If the calculated amount if greater than
     * $400,000 then reduce it to $400,000 or if the calculated amount is less
     * than $50,000 then increase the amount to $50,000
     * 
     * @return - Returns the insurance amount
     */
    public Double calculateInsurance()
    {
        double insurance = calculateEmployeeSalary()
                * INSURANCE_MULTIPLYING_FACTOR;

        if ( insurance < MIN_INSURANCE_AMOUNT )
        {
            insurance = MIN_INSURANCE_AMOUNT;
        }
        else if ( insurance > MAX_INSURANCE_AMOUNT )
        {
            insurance = MAX_INSURANCE_AMOUNT;
        }

        return insurance;
    }
}
