public class EmployeeData
{
    // Use employeeAttributeArray below as �input� data
    public static Object[][] employeeAttributeArray = {
            { "Leonid", "Trusov", "06025", null, 2080, 10.5, null, null, null,
            null, null },
            { "John", "Pfeiffer", "25246", "2/18/1999", 24, 10000.0, 158.0,
            40.0, 8.0, 200.0, 40.0 },
            { "Barbara", "Chevalier", "02628", "11/4/1984", 24, 1000.0, 240.0,
            40.0, 40.0, 240.0, 32.0 },
            { "Sharon", "Rethmeyer", "11817", null, 2080, 15.75, null, null,
            null, null, null },
            { "Aaron", "Diffenderfer", "42017", "4/18/2001", 24, 40600.0,
            132.0, 40.0, 16.0, 160.0, 48.0 },
            { "Debra Sue", "Wood", "08696", "5/7/2012", 24, 33075.0, 0.0, 40.0,
            8.0, 240.0, 80.0 },
            { "Beth", "Vegiard", "03184", "4/25/1985", 24, 57125.0, 240.0,
            40.0, 8.0, 240.0, 56.0 }               };

    // Use employeeTypeArray (employee ID/employee type xref) below as input for
    // HashMap
    public static Object[][] employeeTypeArray      = {
            { "42017", "Non-Manager" }, { "02628", "Manager" },
            { "03184", "Manager" }, { "06025", "Contractor" },
            { "25246", "Non-Manager" }, { "08696", "Non-Manager" },
            { "11817", "Contractor" }

                                                    };

}
