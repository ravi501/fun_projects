import java.math.BigDecimal;
import java.text.DecimalFormat;


public class Utility
{
    final static DecimalFormat MONEY_FORMATTER = new DecimalFormat(
                                                      "$###,###.00" );

    final static DecimalFormat LEAVE_FORMATTER = new DecimalFormat( "####0.00" );

    final static String        YES            = "Y";
    final static String        NO             = "N";
    final static String        NOT_APPLICABLE = "N/A";

    public static String applyCurrencyFormat( Double amount )
    {
        String formattedAmount;
        if ( amount != null )
        {
            BigDecimal decimalAmount = new BigDecimal( Double.toString( amount ) );
            formattedAmount = MONEY_FORMATTER.format( decimalAmount );
        }
        else
        {
            formattedAmount = NOT_APPLICABLE;
        }
        return formattedAmount;
    }

    public static String applyDecimalFormat( Double balance )
    {
        String formattedLeaves;
        if ( balance != null )
        {
            BigDecimal decimalAmount = new BigDecimal( Double.toString( balance ) );
            formattedLeaves = LEAVE_FORMATTER.format( decimalAmount );
        }
        else
        {
            formattedLeaves = NOT_APPLICABLE;
        }
        return formattedLeaves;
    }

    public static String checkBenefitEligibility( Boolean benefit )
    {
        String getStatus;

        if ( benefit == null )
        {
            getStatus = NOT_APPLICABLE;
        }
        else if ( benefit )
        {
            getStatus = YES;
        }
        else
        {
            getStatus = NO;
        }
        return getStatus;
    }

    public static String processHireDate( String date )
    {
        String hireDate;

        if ( date == null )
        {
            hireDate = NOT_APPLICABLE;
        }
        else
        {
            hireDate = date;
        }
        return hireDate;
    }
}
