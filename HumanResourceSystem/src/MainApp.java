public class MainApp
{

    public static void main( String[] args )
    {
        HumanResourceSystem system = HumanResourceSystem.getInstance();

        system.process();
    }
}
