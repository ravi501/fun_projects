public abstract class Employee
{
    // Employee class stores the individual employee details

    // Stores the first name of the employee
    private String      employeeFirstName;

    // Stores the last name of the employee
    private String      employeeLastName;

    // Stores the employee ID
    private String      employeeID;

    // Stores the type of the employee
    private String      employeeType;

    // Stores the hire date of the employee
    private String      hireDate;

    // Stores the salary period
    private Integer     salaryPeriod;

    // Stores the salary rate per period
    private Double      salaryRate;

    // Stores the length of employment for the employees
    private Double      employmentLength;

    // Stores the sick time earned
    private Double      sickTimeEarned;

    // Stores the sick time taken
    private Double      sickTimeTaken;

    // Stores the vacation time earned
    private Double      vacationEarned;

    // Stores the vacation time taken
    private Double      vacationTaken;

    // Stores the retirement eligibility of the employee
    private Boolean     retirementEligibility;

    // Stores the high compensation details of the employee
    private Boolean     highCompensation;

    // Constants to store the non-manager, manager and contract employee string
    // values
    final static String NONMANAGER_EMPLOYEE_TYPE = "Non-Manager";
    final static String MANAGER_EMPLOYEE_TYPE    = "Manager";
    final static String CONTRACTOR_EMPLOYEE_TYPE = "Contractor";

    // Constant values used for accessing the individual employee attributes
    // from the array
    final static int    EMPLOYEE_FIRST_NAME      = 0;
    final static int    EMPLOYEE_LAST_NAME       = 1;
    final static int    EMPLOYEE_ID              = 2;
    final static int    EMPLOYEE_HIRE_DATE       = 3;
    final static int    EMPLOYEE_SALARY_PERIOD   = 4;
    final static int    SALARY_RATE_PERIOD       = 5;
    final static int    LENGTH_OF_EMPLOYMENT     = 6;
    final static int    SICK_TIME_EARNED         = 7;
    final static int    SICK_TIME_TAKEN          = 8;
    final static int    VACATION_EARNED          = 9;
    final static int    VACATION_TAKEN           = 10;

    // Abstract method to process the benefits
    public abstract Double calculateInsurance();

    public Integer getSalaryPeriod()
    {
        return salaryPeriod;
    }

    public void setSalaryPeriod( Integer salaryPeriod )
    {
        this.salaryPeriod = salaryPeriod;
    }

    public String getEmployeeLastName()
    {
        return employeeLastName;
    }

    public void setEmployeeLastName( String employeeLastName )
    {
        this.employeeLastName = employeeLastName;
    }

    public Double getSalaryRate()
    {
        return salaryRate;
    }

    public void setSalaryRate( Double salaryRate )
    {
        this.salaryRate = salaryRate;
    }

    public Double getEmploymentLength()
    {
        return employmentLength;
    }

    public void setEmploymentLength( Double employmentLength )
    {
        this.employmentLength = employmentLength;
    }

    public Double getSickTimeEarned()
    {
        return sickTimeEarned;
    }

    public void setSickTimeEarned( Double sickTimeEarned )
    {
        this.sickTimeEarned = sickTimeEarned;
    }

    public Double getSickTimeTaken()
    {
        return sickTimeTaken;
    }

    public void setSickTimeTaken( Double sickTimeTaken )
    {
        this.sickTimeTaken = sickTimeTaken;
    }

    public Double getVacationEarned()
    {
        return vacationEarned;
    }

    public void setVacationEarned( Double vacationEarned )
    {
        this.vacationEarned = vacationEarned;
    }

    public Double getVacationTaken()
    {
        return vacationTaken;
    }

    public void setVacationTaken( Double vacationTaken )
    {
        this.vacationTaken = vacationTaken;
    }

    public String getEmployeeFirstName()
    {
        return employeeFirstName;
    }

    public void setEmployeeFirstName( String employeeFirstName )
    {
        this.employeeFirstName = employeeFirstName;
    }

    public String getEmployeeID()
    {
        return employeeID;
    }

    public void setEmployeeID( String employeeID )
    {
        this.employeeID = employeeID;
    }

    public String getHireDate()
    {
        return hireDate;
    }

    public void setHireDate( String hireDate )
    {
        this.hireDate = hireDate;
    }

    public String getEmployeeType()
    {
        return employeeType;
    }

    public void setEmployeeType( String employeeType )
    {
        this.employeeType = employeeType;
    }

    public Boolean getRetirementEligibility()
    {
        return retirementEligibility;
    }

    public void setRetirementEligibility( Boolean retirementEligibility )
    {
        this.retirementEligibility = retirementEligibility;
    }

    public Boolean getHighCompensation()
    {
        return highCompensation;
    }

    public void setHighCompensation( Boolean highCompensation )
    {
        this.highCompensation = highCompensation;
    }

    /**
     * Returns the specific employee object corresponding to the current
     * employee record
     * 
     * @param employeeType
     *            - Takes the employee type as the input
     * @return - Returns the specific Employee object
     */
    public static Employee getEmployeeType( String employeeType )
    {
        Employee employee = null;

        if ( NONMANAGER_EMPLOYEE_TYPE.equals( employeeType ) )
        {
            employee = new NonManager();
        }
        else if ( MANAGER_EMPLOYEE_TYPE.equals( employeeType ) )
        {
            employee = new Manager();
        }
        else if ( CONTRACTOR_EMPLOYEE_TYPE.equals( employeeType ) )
        {
            employee = new ContractEmployee();
        }

        return employee;
    }

    /**
     * Loads the basic attributes of the employee object common to contractor,
     * manager and non-manager
     * 
     * @param i
     */
    public void build( EmployeeParser parser )
    {

        setEmployeeFirstName( parser.getEmployeeFirstName() );
        setEmployeeLastName( parser.getEmployeeLastName() );
        setEmployeeID( parser.getEmployeeID() );
        setHireDate( parser.getHireDate() );
        setSalaryPeriod( parser.getSalaryPeriod() );
        setSalaryRate( parser.getSalaryRate() );
        setEmploymentLength( parser.getEmploymentLength() );
        setSickTimeEarned( parser.getSickTimeEarned() );
        setSickTimeTaken( parser.getSickTimeTaken() );
        setVacationEarned( parser.getVacationEarned() );
        setVacationTaken( parser.getVacationTaken() );
    }

    public Double calculateEmployeeSalary()
    {
        return (getSalaryPeriod() * getSalaryRate());
    }

    public Double calculateSickRemain()
    {
        Double sickTimeRemain = null;

        if ( getSickTimeEarned() != null )
        {
            sickTimeRemain = getSickTimeEarned() - getSickTimeTaken();
        }
        return sickTimeRemain;
    }

    public Double calculateVacationRemain()
    {
        Double vacationRemain = null;

        if ( getVacationEarned() != null )
        {
            vacationRemain = getVacationEarned() - getVacationTaken();
        }

        return vacationRemain;
    }

}
