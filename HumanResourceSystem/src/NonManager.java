public class NonManager extends FullTimeEmployee
{
    /**
     * Process Benefits method for the non-manager
     */
    public void build( EmployeeParser parser )
    {
        // Calls the process benefits of the Full Time Employee class
        super.build( parser );

        // Sets the HCE as not applicable for non-manager
        setEmployeeType( NONMANAGER_EMPLOYEE_TYPE );

    }

}
