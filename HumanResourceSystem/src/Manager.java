public class Manager extends FullTimeEmployee
{
    // Constants specific to Manager class
    final static double HCE_SALARY_LIMIT = 115000;

    /**
     * Processes the benefits of the Manager
     */
    public void build( EmployeeParser parser )
    {
        // Calls the process benefits of the Full Time Employee class
        super.build( parser );
        setEmployeeType( MANAGER_EMPLOYEE_TYPE );
        setHighCompensation( getCompensation() );
    }

    /**
     * Processes the high compensation for the employee
     */
    private Boolean getCompensation()
    {
        // Checks if the employee salary if greater than $115000 or not and
        // based on it sets the High Compensation
        Boolean compensation;

        if ( calculateEmployeeSalary() > HCE_SALARY_LIMIT )
        {
            compensation = Boolean.TRUE;
        }
        else
        {
            compensation = Boolean.FALSE;
        }
        return compensation;
    }
}
